module Philosophers where
 
import Control.Concurrent
import Control.Concurrent.STM
import System.Random
import Control.Monad

type Fork = TMVar Int
 
newFork :: Int -> IO Fork
newFork i = newTMVarIO i
 
grabFork :: Fork -> STM Int
grabFork fork = takeTMVar fork
 
putDownFork :: Int -> Fork -> STM ()
putDownFork i fork = putTMVar fork i
 
type Name = String
 
runPhilosopher :: Name -> (Fork, Fork) -> IO ()
runPhilosopher name (left, right) = forever $ do
  putStrLn (name ++ " is hungry.")
 
  (leftForkNum, rightForkNum) <- atomically $ do
    leftForkNum <- grabFork left
    righForkNum <- grabFork right
    return (leftForkNum, rightForkNum)
 
  putStrLn (name ++ " got forks " ++ show leftForkNum ++ " and " ++ show righForkNum ++ " and now is eating.")

  delay <- randomRIO (1,5)
  threadDelay (delay * 1500000) 
  putStrLn (name ++ " finished eating. Start thinking again.")
 
  atomically $ do
    putDownFork leftForkNum left
    putDownFork rightForkNum right
 
  delay <- randomRIO (1, 5)
  threadDelay (delay * 1500000)
 
philosophers :: [String]
philosophers = ["Plato", "Socrates", "Aristotle", "Euclid", "Heraclitus"]
 
main = do
  forks <- mapM newFork [1..5]
  let namedPhilosophers  = map runPhilosopher philosophers
      forkPairs          = zip forks (tail . cycle $ forks)
      philosophersWithForks = zipWith ($) namedPhilosophers forkPairs
  
  mapM_ forkIO philosophersWithForks
 
  getLine